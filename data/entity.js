class Entity {
	constructor(type){
		this.type = type;
	}

	/**
	 * Find the IDs of all entities for the current type
	 * @returns {AsyncIterable<String>} Iterable of IDs
	 */
	async *index(){
		for await (const [id] of this.list()) {
			yield id;
		}
	}

	/**
	 * Find all entities for the current type
	 * @returns {AsyncIterable<[String, Object]>} Iterable of [ID, Object] items
	 */
	async *list(){
		for await(const id of this.index()) {
			yield [
				id,
				await this.get(id)
			]
		}
	}

	/**
	 * Add a new entity of this type
	 * @param {Object} object The object to create
	 * @async
	 * @returns {String} ID of the created object
	 */
	post(object){
		throw new Error('Not implemented');
	}

	/**
	 * Return the entity of the given ID, or the first entity of this type if ID is nullish
	 * @param {String} [id] The ID to fetch, or null
	 * @async
	 * @returns {Object} entity
	 */
	async get(id) {
		const items = this.list();

		for await (const [itemId, item] of items) {
			if(!id || itemId === id) {
				return item;
			}
		}
	}

	/**
	 * Replace an object with the given ID with a the given object
	 * @param {String} id The ID of the object
	 * @param {Object} object The new object
	 * @async
	 * @returns {Object} The object
	 */
	put(id, object) {
		throw new Error('Not implemented');
		return object;
	}

	/**
	 * Delete an object
	 * @param {String} id The ID of the object to delete
	 * @async
	 */
	delete(id) {
		throw new Error('Not implemented');
	}

	/**
	 * Update properties on the object with the same ID
	 * @param {String} id The ID of the object to update
	 * @param {Object} object The properties to patch
	 * @async
	 */
	patch(id, object) {
		throw new Error('Not implemented');
	}
}


module.exports = Entity;
