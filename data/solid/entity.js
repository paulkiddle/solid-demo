const N3 = require('n3');
const Entity = require('../entity');

module.exports = class SolidData extends Entity {
	#client;
	#typeUrl;
	#defaultReg;

	constructor(type, defaultReg, client) {
		super(type);

		if(typeof defaultReg === 'string'){
			defaultReg = {
				path: defaultReg,
				collection: false,
				private: true
			}
		}
		this.#defaultReg = defaultReg;

		this.#client = client;
	}

	async getTypeUrl(){
		if(this.#typeUrl){
			return this.#typeUrl;
		}

		const types = await this.#client.lookupTypes();
		let url;

		if(this.type in types) {
			url = types[this.type];
		} else {
			if(!this.#defaultReg.path) {
				const e = new TypeError('No default registration path given for ', this.type);
				throw e;
			}
			console.log('No type', this.type, 'in', this.types);
			url = await this.#client.registerType(this.type, this.#defaultReg.path, this.#defaultReg.collection, this.#defaultReg.private);
			if(this.#defaultReg.collection) {
				const dummy = this.#defaultReg.path + '/.dummy';
				await this.put(dummy);
				await this.delete(dummy);
			}
		}

		return this.#typeUrl = url;
	}

	async *index(){
		const url = await this.getTypeUrl();
		console.log('Type url', url);
		if(Array.isArray(url)) {
			yield* url;
			return;
		} else {
			const res = await this.#client.fetch(url);
			if(res.status >= 400) {
				console.log(res);
				return;
			}
			const streamParser = new N3.StreamParser({
				baseIRI: url
			});
			for await(const quad of res.body.pipe(streamParser)){
				if((quad.predicate.value === 'http://www.w3.org/1999/02/22-rdf-syntax-ns#type') &&
					(quad.object.value === 'http://www.w3.org/ns/ldp#Resource')) {
					yield quad.subject.value;
				}
			}
		}
	}

	async get(url) {
		const o = {};
		if(!url) {
			const nextItem = await this.index().next();
			console.log('Next item', nextItem);
			url = nextItem.value;
		}

		console.log('Fetch url', url);

		try {
			await this.#client.fetchTtl(url, quad => {
				console.log('Fetch complete', url);
				if(!quad) return;
				if(quad.subject.value === url) {
					o[quad.predicate.value] = quad.object.value;
				}
			})
		} catch(e) { }
		return o;
	}

	ttl(object) {
		return `<>\n\ta <${this.type}>;\n\t${
			Object.entries(object).map(([p, o]) => `<${p}> "${o}"`).join(';\n\t')
		}.`;
	}

	async post(object){
		const url = await this.getTypeUrl();
		const body = object && this.ttl(object);

		if(Array.isArray(url)) {
			await this.put(url[0], object);
			return url[0];
			//throw new Error(`Can't post, type registration ${this.type} is not a collection`);
		}

		const res = await this.#client.fetch(
			url,
			{
				method: 'POST',
				headers: {
					'Content-Type': 'text/turtle'
				},
				body
			}
		);
		return new URL(res.headers.get('location'), url).toString();
	}

	async put(url, object) {
		const body = this.ttl(object);
		const res = await this.#client.fetch(
			url,
			{
				method: 'PUT',
				headers: {
					'Content-Type': 'text/turtle'
				},
				body
			}
		);
		return res;
	}

	async delete(url) {
		const res = await this.#client.fetch(
			url,
			{
				method: 'DELETE'
			}
		);
		return res;
	}

	acl(){
		return true;
	}

}
