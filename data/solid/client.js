const { customAuthFetcher } = require("solid-auth-fetcher");
const { default: fetch } = require("node-fetch");
const N3 = require('n3');

const ns = base => term => base+term;
const solid = ns('http://www.w3.org/ns/solid/terms#');
const terms = ns('http://purl.org/dc/terms/');

class SolidClient {
	#server;
	#token;
	#appUrl;
	#podRoot;
	#typeIndexes;
	#typeIndex;

	constructor(podRoot, token, server, appUrl = "http://localhost"){
		this.#token = token;
		this.#podRoot = podRoot;
		this.#appUrl = appUrl;
		this.#server = server;
	}

	static async login(server, username, password) {
		const {token, pod} = await this.genToken(server, username, password);

		return new this(pod, token, server);
	}

	static async genToken(serverRoot, username, password) {
		const authFetcher = await customAuthFetcher();
		const serverLoginResult = await authFetcher.fetch(
			`${serverRoot}/login/password`,
			{
				headers: {
					"content-type": "application/x-www-form-urlencoded"
				},
				body: `username=${username}&password=${password}`,
				method: "POST",
				redirect: "manual"
			}
		);
		return {
			token: serverLoginResult.headers.get("set-cookie"),
			pod: serverLoginResult.headers.get('location')
		};
	}

	async getRoot(){
		return this.#podRoot;
	}

	async getTypeIndexes(){
		if(this.#typeIndexes){
			return this.#typeIndexes;
		}

		const root = await this.getRoot();
		const account = await this.fetchTtl(root, quad => {
			if(quad && quad.predicate.value === solid('account'))  {
				return quad.subject.value;
			}
		});

		const typeIndexes = {};
		await this.fetchTtl(account, quad => {
			if(quad && quad.predicate.value === solid('privateTypeIndex')) {
				typeIndexes.private = quad.object.value;
			} else if(quad && quad.predicate.value === solid('publicTypeIndex')) {
				typeIndexes.public = quad.object.value;
			}
		});
		return this.#typeIndexes = typeIndexes;
	}

	async lookupTypes(){
		if(this.#typeIndex){
			return this.#typeIndex;
		}
		const typeIndexes = Object.values(await this.getTypeIndexes());
		const refs = {};
		for(const ix of typeIndexes) {
			await this.fetchTtl(ix, quad => {
				if(quad) {
					if(quad.object.value === solid('TypeRegistration')) {
						refs[quad.subject.value] = {};
					}
					if(quad.predicate.value === solid('forClass')) {
						refs[quad.subject.value].class = quad.object.value;
					}
					if(quad.predicate.value === solid('instance')) {
						refs[quad.subject.value].instances = [quad.object.value];
					}
					if(quad.predicate.value === solid('instanceContainer')) {
						refs[quad.subject.value].instances = quad.object.value;
					}
				}
			});
		}
		return this.#typeIndex = Object.fromEntries(Object.values(refs).map(k => [k.class, k.instances]));
	}

	async registerType(type, location, container=false, isPrivate = true){
		const typeIndexes = await this.getTypeIndexes();
		const url = (typeIndexes)[isPrivate?'private':'public'];
		location = new URL(location, this.#podRoot).toString();
		const instance = container ? solid('instanceContainer') : solid('instance');

		const quad = `
<${url}> <${terms('references')}> [
	a <${solid('TypeRegistration')}> ;
	<${solid('forClass')}> <${type}> ;
	<${instance}> <${location}> ] .
`;

		// Todo: Should be able to do a SPARQL insert but it doesn't seem to have any effect
		/*

		const insert = `INSERT DATA { ${quad} }`;

		const res = await this.fetch(url, {
			method: 'PATCH',
			headers: {
				'Content-Type': 'application/sparql-update'
			}
		}, {
			body: insert
		});
		*/

		// Instead try read + append + put
		const doc = await this.fetch(url, { headers: { accept: 'text/turtle' } });
		const body = await doc.text() + quad;

		await this.fetch(url, { headers: { 'content-type': 'text/turtle' }, method: 'PUT', body });

		return location;
	}

	async fetchTtl(url, options = {}, cb = ()=>{}) {
		if(options instanceof Function) {
			cb = options;
			options = {};
		}

		const res = await this.fetch(url, {
			headers: {
				'Content-Type': 'text/turtle',
				'Accept': 'text/turtle'
			},
			...options
		});

		if(res.status >= 400) {
			const e = new Error(`Fetch failed for ${url}`);
			e.res = res;
			throw e;
		}

		const parser = new N3.Parser({
			baseIRI: url
		});

		return new Promise((resolve, reject) =>
			parser.parse(res.body, (err, quad, pref) => {
				if(err) return reject(err);

				const rtn = cb(quad, pref);
				if(rtn || pref) {
					resolve(rtn);
				}
			}));
	}

	async fetch(url, options) {
		const serverRoot = this.#server;
		const cookie = await this.#token;
		const appRedirectUrl = this.#appUrl;

		const authFetcher = await customAuthFetcher();
		const session = await authFetcher.login({
			oidcIssuer: serverRoot,
			redirect: appRedirectUrl
		});
		let redirectedTo = session.neededAction.redirectUrl;
		do {
			const result = await fetch(redirectedTo, {
				headers: {
					cookie
				},
				redirect: "manual"
			});
			redirectedTo = result.headers.get("location");
			// console.log("Redirected to", redirectedTo);
		} while (!redirectedTo?.startsWith(appRedirectUrl));

		await authFetcher.handleRedirect(redirectedTo);
		url = new URL(url, this.#podRoot).toString();
		return authFetcher.fetch(url, options);
	}
}

module.exports = SolidClient;
