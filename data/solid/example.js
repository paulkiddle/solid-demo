const SolidClient = require('./client');
const SolidEntity = require('./entity');
const ns = a => b => a + b;
const MANIFEST = 'https://www.w3.org/TR/2020/WD-appmanifest-20200715/#webappmanifest-dictionary';
const site = ns('https://www.w3.org/TR/2020/WD-appmanifest-20200715/#dom-webappmanifest-');
const defaultPath = 'scratchpads/site-settings.ttl';

const [server, username, password] = process.argv.slice(2);

console.log({server, username, password});

SolidClient.login(server, username, password).then(
	async client => {
		const manifest = new SolidEntity(MANIFEST, defaultPath, client);

		console.log('Fetch existing manifest...');

		const settings = await manifest.get();

		console.log('Manifest:', settings);

		console.log('Name: ', settings[site('name')]);
		console.log('Colour: ', settings[site('theme_color')]);

		console.log('Update the colour...');

		settings[site('theme_color')] = '#' + Array(6).fill().map(()=>Math.floor(Math.random()*16).toString(16)).join('');

		const file = await manifest.post(settings);

		console.log('Updated ' + file)
	}
);
